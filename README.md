# Github Repository App

This is an app showing github repositories with the total stars. One can also check the details of the repository by going to the details page. 

## Features
- Total repository count
- Repository list with total stars
- Sorting option
- Repository details with owner info and avatar
- Repositories can be checked even in offline

## Tools & Technologies
- Language: Kotlin
- Clean Architecture with MVVM
- Corouine
- Flow
- Dependency Injection(using Hilt)
- Network Concurrency

## Unfinished Tasks
- Pagination added but not working
- UI testing
- Unit testing
- Flavour
